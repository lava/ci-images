#!/bin/sh

set -e

echo "Clone lava.git"
git clone https://gitlab.com/lava/lava.git /root/lava
cd /root/lava
echo "done"
echo

echo "Check bullseye dependencies"
PACKAGES=$(./share/requires.py -p lava-dispatcher -d debian -s bullseye -n)
for pkg in $PACKAGES
do
  echo "* $pkg"
  dpkg -l "$pkg" > /dev/null
done

echo "Run dispatcher test suite"
.gitlab-ci/test/amd64/dispatcher-debian-12.sh
echo "done"
