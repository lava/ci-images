#!/bin/sh

set -e

echo "Clone lava.git"
git clone https://gitlab.com/lava/lava.git /root/lava
cd /root/lava
echo "done"
echo

echo "Check buster dependencies"
PACKAGES=$(./share/requires.py -p lava-dispatcher -d debian -s bullseye -n)
for pkg in $PACKAGES
do
  echo "* $pkg"
  dpkg -l "$pkg" > /dev/null
done

echo "Run dispatcher test suite"
.gitlab-ci/test/aarch64/dispatcher-debian-11.sh
echo "done"
