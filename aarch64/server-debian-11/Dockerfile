FROM debian:bullseye

LABEL maintainer="Rémi Duraffort <remi.duraffort@linaro.org>"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q
RUN apt-get install --no-install-recommends --yes python3-sentry-sdk python3-voluptuous python3-yaml
RUN apt-get install --no-install-recommends --yes adduser apache2 gunicorn3 iproute2 libjs-bootstrap libjs-excanvas libjs-jquery libjs-jquery-cookie libjs-jquery-flot libjs-jquery-typeahead libjs-jquery-ui libjs-jquery-watermark postgresql postgresql-client postgresql-common python3-setuptools systemd-sysv
RUN apt-get install --no-install-recommends --yes python3-aiohttp python3-asgiref python3-celery python3-defusedxml python3-django python3-django-allauth python3-django-auth-ldap python3-django-environ python3-django-filters python3-django-tables2 python3-djangorestframework python3-djangorestframework-extensions python3-djangorestframework-filters python3-jinja2 python3-junit.xml python3-psycopg2 python3-requests python3-tap python3-voluptuous python3-whitenoise python3-yaml python3-zmq
RUN apt-get install --no-install-recommends --yes python3-configobj python3-guestfs python3-magic python3-netifaces python3-pexpect python3-pymongo python3-pytest python3-pytest-cov python3-pytest-django python3-pytest-mock python3-pytest-random-order python3-pytest-subtests python3-pyudev telnet tftpd-hpa u-boot-tools
RUN apt-get install --no-install-recommends --yes bmap-tools python3-aiohttp python3-configobj python3-guestfs python3-jinja2 python3-magic python3-netifaces python3-pexpect python3-pyudev python3-requests python3-setproctitle python3-yaml
RUN apt-get install --no-install-recommends --yes android-sdk-libsparse-utils docker.io gdb-multiarch git lxc minicom openocd python3-pyocd python3-pytest python3-pytest-mock python3-pytest-random-order python3-pytest-subtests python3-responses python3-voluptuous qemu-system-arm qemu-system-x86 schroot telnet tftpd-hpa u-boot-tools
RUN apt-get install --no-install-recommends --yes git nfs-kernel-server sudo python3-pytest-xdist

RUN apt-get install --no-install-recommends --yes python3-pip && \
    python3 -m pip install  avh-api==1.0.5

COPY entrypoint.sh /root/
ENTRYPOINT ["/root/entrypoint.sh"]
